#!/usr/bin/env python
#usage: sudo twistd -ny service.py

#service for interfacing between ROS and Android Phone

import sys, uuid, time, commands, os
from twisted.application import internet, service
from twisted.internet import defer, reactor, protocol, threads, task, utils
from twisted.protocols import basic
from twisted.python import log

        
class RobotServerProtocol(basic.LineReceiver):
    def connectionMade(self):
        self.local_ip = self.transport.getHost().host
        self.remote_ip = self.transport.getPeer().host
        log.msg("connection open from: %s" % str(self.remote_ip))
        self.factory.clients.add(self)
        
    def sendData(self, msg):
        log.msg("sending update to prev_hop: " + str(msg))
        self.transport.write(msg + '\r\n')
        
    def connectionLost(self, reason):
        log.msg("Connection close: " + str(self.remote_ip) + " Reason: " + str(reason.getErrorMessage()))
        self.factory.clients.remove(self)
    
    def lineReceived(self, line):
        params = line.split()
        command, params = params[0].lower(), params[1:]
        if command == "ACK":
            return
        log.msg("Recieved command: " + str(command) + " params: " + str(params))
        try: 
            d = threads.deferToThread(self.factory.commands[command], self, *params)

            def onError(err):
                return ["ERROR", str(err)]
            d.addErrback(onError)
            
            def writeResponse(messages):
                for message in messages:
                    self.transport.write(message + '\r\n')
            d.addCallback(writeResponse)
            
        except Exception, err:
            log.msg("Unrecognized command: " + str(err))
            self.transport.write("ERROR\r\n")
            self.transport.loseConnection()



class RobotService(service.Service):    
    def __init__(self):
        self._apps = {}
        self._links = {}
        self.clients = set()
        self.task = task.LoopingCall(self._update)

    def getRobotServerFactory(self):
        f = protocol.ServerFactory()
        f.protocol = RobotServerProtocol
        f.commands = {
            'test': self.test,
            'test2': self.test2,
            'do': self.do            
        }
        f.clients = self.clients
        return f
        
    def _update(self):
        for link_id, link in self._links.items():
            link.update()
            link.notifyPeers()
        for app_id, app in self._apps.items():
            app.updateLinks()
        
    def startService(self):
        self.task.start(1.0)
        service.Service.startService(self)

    def stopService(self):
        self.task.stop()
        service.Service.stopService(self)
        self.call.cancel()
 
    def test(self, connection):
        output = []
        output.append("# of active links: " + str(len(self.clients)))       
        return output
        
    def test2(self,connection, word):
        log.msg("test: " + word)
        return ["ACK"]
        
    def do(self, connection, *command):
        exec(str(' '.join(command)))
        return ["ACK"]


application = service.Application('robot', uid=1, gid=1) 
f = RobotService()
f.setServiceParent(service.IServiceCollection(application))
internet.TCPServer(3500, f.getRobotServerFactory()).setServiceParent(service.IServiceCollection(application))
